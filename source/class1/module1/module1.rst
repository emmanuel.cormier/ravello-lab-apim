Module 1 - Reviewing the Arcadia application
--------------------------------------------

In this module, we will focus on the **Arcadia application**
in **Kubernetes** cluster

We will do the following labs:

.. toctree::
   :maxdepth: 1
   :glob:

   lab*
